<?php

use App\User;
use App\Admin;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $users = factory(User::class, 10)->create();
        User::create([
        	'name' 		=> 'User',
        	'email' 	=> 'user@user.com',
        	'password' 	=> bcrypt(123456),
        	'address' 	=> 'mohamed address goes here!',
        	'mobile' 	=> '01026687240',
        	'type' 		=> 'user'
        ]);

        User::create([
        	'name' 		=> 'Doctor',
        	'email' 	=> 'doctor@user.com',
        	'password' 	=> bcrypt(123456),
        	'address' 	=> 'doctor address goes here!',
        	'mobile' 	=> '01026687240',
        	'type' 		=> 'doctor'
        ]);

        Admin::create([
            'name'      => 'Super Admin',
            'email'     => 'super@admin.com',
            'password'  => '123456',
            'type'      => 'super'
        ]);

        Admin::create([
            'name'      => 'Normal Admin',
            'email'     => 'normal@admin.com',
            'password'  => '123456',
            'type'      => 'normal'
        ]);
    }
}
