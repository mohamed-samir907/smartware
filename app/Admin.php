<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Admin extends Authenticatable
{
    use Notifiable;

    /**
     * The Model Guard
     * 
     * @var string
     */
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $guard = 'admin';
    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',  'type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

public function setPasswordAttribute($value)
{
    $this->attributes['password'] = bcrypt($value);
}
}