<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name', 'email', 'password', 'address', 'mobile', 'telephone', 'type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the Rochtas for the Doctor
     * 
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function rochtas()
    {
        if ($this->type == 'doctor') {
            return $this->hasMany('App\Model\Rochta');
        }
    }
    public function setPasswordAttribute($value)
{
    $this->attributes['password'] = bcrypt($value);
}
}
