<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Illness extends Model
{
    /**
     * The Attributes that are mass assignable
     * 
     * @var array
     */
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
    	'name', 'description'
    ];

    /**
     * Get the Drugs for the Illness
     * 
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function drugs()
    {
    	return $this->belongsToMany('App\Model\Drug');
    }
}
