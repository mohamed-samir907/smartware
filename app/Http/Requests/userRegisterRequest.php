<?php

namespace App\Http\Requests;
use App\User;
use Illuminate\Foundation\Http\FormRequest;

class userRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users',
            'name' => 'required|string|max:50',
            'password' => 'required',
            'address'  => 'required',
            'mobile' => 'required|regex:/(01)[0-9]{9}/',
            'telephone' =>  'required|numeric' ,
            'type' => 'required',
        ];
    }
// //     public function withValidator($validator)
// // {
// //     $validator->after(function ($validator) {
// //         if ($this->email()) {
// //             $validator->errors()->add('email', 'Something is wrong with email!');
// //         }
// //         if ($this->name()) {
// //             $validator->errors()->add('name', 'Something is wrong with name!');
// //         }
// //         if ($this->passwrod()) {
// //             $validator->errors()->add('password', 'Something is wrong with password!');
// //         }
// //         if ($this->mobile()) {
// //             $validator->errors()->add('mobile', 'Something is wrong with mobile!');
// //         }
// //         if ($this->address()) {
// //             $validator->errors()->add('address', 'Something is wrong with address!');
// //         }
// //         if ($this->telephone()) {
// //             $validator->errors()->add('telephone', 'Something is wrong with telephone!');
// //         }
// //     });
   
// // }
// public function messages()
// {
//     return [
//         // Here we explicitly define the message and the attribute
//         'email.required' => 'The user requires a email',
//         // Here we let Laravel to fill the placeholder with the name of the attribute
//         'name.required'  => 'The :name field is required',
//         'mobile.required'  => 'The :mobile field is required',
//         'address.required'  => 'The :address field is required',
//         'telephone.required'  => 'The :telephone field is required',
//         'type.required'  => 'The :type field is required',

//     ];
// }
}

