<?php

namespace App\Http\Controllers\Illness;
use App\Model\Illness;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response;
// use App\Http\Requests\IllnessRequest;

class IllnessController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //
      $illness =Illness::latest()->paginate(5);
      return view('illness.index',compact('illness'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        // return view('illness.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax()){
        $illness = Illness::create($request->all());
        return response($illness); 
            
    }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $illness = Illness::findOrFail($id);

        return view('illness.show',compact('illness'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request  $request, $id)
    {
        
        if($request->ajax()){
            $illness = Illness::findOrFail($id);
 
            return Response::json($illness);
   
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        if($request->ajax()){
      
            $illness = Illness::find($id);
           $illness= $illness->update($request->all());
       
            return response($illness); 
           
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // if($request->ajax()){
            
        //    illness::destroy($request->id);

        // return response()->json(['message'=>'illness deleted successfully']);}
        Illness::find($id)->delete($id);
  
        return response()->json([
            'success' => 'Record deleted successfully!'
        ]);
            
   
}
}