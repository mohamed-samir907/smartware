<?php

namespace App\Http\Controllers\Admin\Users;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response;
use App\Http\Requests\userRegisterRequest;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //
      $users =User::latest()->paginate(5);
      return view('admin.users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        // return view('userRegister.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(userRegisterRequest $request)
    {
        if($request->ajax()){
        $users = User::create($request->all());
        return response($users); 
            
    }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $user = User::findOrFail($id);

        return view('userRegister.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request  $request, $id)
    {
        
        if($request->ajax()){
            $user = User::findOrFail($id);
 
            return Response::json($user);
   
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        if($request->ajax()){
      
            $user = User::find($id);
           $user= $user->update($request->all());
       
            return response($user); 
           
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // if($request->ajax()){
            
        //    user::destroy($request->id);

        // return response()->json(['message'=>'user deleted successfully']);}
        User::find($id)->delete($id);
  
        return response()->json([
            'success' => 'Record deleted successfully!'
        ]);
            
   
}
}