<?php

namespace App\Http\Controllers\Admin\Admins;
use App\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response;
use App\Http\Requests\adminRegisterRequest;

class AdminController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //
      $admins =Admin::latest()->paginate(5);
      return view('admin.admins.index',compact('admins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        // return view('adminRegister.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(adminRegisterRequest $request)
    {
        if($request->ajax()){
        $admins = Admin::create($request->all());
        return response($admins); 
            
    }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $admin = Admin::findOrFail($id);

        return view('adminRegister.show',compact('admin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request  $request, $id)
    {
        
        if($request->ajax()){
            $admin = Admin::findOrFail($id);
 
            return Response::json($admin);
   
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        if($request->ajax()){
      
            $admin = Admin::find($id);
           $admin= $admin->update($request->all());
       
            return response($admin); 
           
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // if($request->ajax()){
            
        //    Admin::destroy($request->id);

        // return response()->json(['message'=>'Admin deleted successfully']);}
        Admin::find($id)->delete($id);
  
        return response()->json([
            'success' => 'Record deleted successfully!'
        ]);
            
   
}
}