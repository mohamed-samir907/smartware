<?php

namespace App\Http\Controllers\Advices;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Advice;
use App\Model\Rochta;


class AdviceRochtaController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax()){
            $rochta = Rochta::find($rochta_id);
            $advices= $rochta-> advices()->attach($advice_id);
            return response($advices); 
    }
}
  
  

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rochta = Rochta::find($rochta_id);
        $advices= $rochta-> advices()->detach($advice_id);
        return response()->json([
            'success' => 'Record deleted successfully!'
        ]);
    }
}
