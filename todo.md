# Database Tables

# Admins
----- ID
----- Name
----- Email
----- Password
----- Type 			['super', 'normal']

## Users
----- ID
----- Name
----- Email
----- Password
----- Address
----- Mobile
----- Telephone
----- Type 			['user', 'doctor']

## Patients
----- ID
----- Name
----- Age
----- Job
----- Mobile 
----- Telephone
----- Gender
----- Address
----- Email 
----- 

## Drugs
----- ID
----- Name
----- Description

## Illnesses
----- ID
----- Name
----- Description

## Illness_Drugs
----- ID
----- Drug_id
----- Illness_id

# Reservations
----- ID
----- Patient_id

## Advices
----- ID
----- Advice

## Rochta
----- ID
----- Doctor_id
----- Notes

## Rochta_advices
----- ID
----- Rocheta_id
----- Advice_id

## Rocheta_drugs
----- ID
----- Rocheta_id
----- Drug_id

# Reservation_rochta
----- ID
----- Reservation_id
----- Rochta_id