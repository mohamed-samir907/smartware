@extends('dashboard.app')
@section('page-title')
Current Admins
@endsection

@section('content')
<div class="container">
<h2>Admin Dashboard</h2>
  <!-- Trigger the modal with a button -->
  <div>
  <button type="button" class="btn btn-info btn-lg" id="showAddModal">Add New Admin</button>
  </div>
  <div >
  <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>NAME</th>
                        <th>type </th>
                        <th>EMAIL </th>
                        <th>UPDATED AT</th>
                        <th>ACTIONS</th>
                    </tr>
                </thead>
                <tbody id="ajaxTable" >
                @php $i = 0 @endphp
                    @foreach($admins as $admin)
                    @php $i++ @endphp
                    <tr id="tr{{$admin->id}}" >
                   
                        <td> {{$admin->id}}</td>
                        <td>{{$admin->name}}</td>
                        <td>{{$admin->type}}</td>
                        <td>{{$admin->email}}</td>
                        <td>{{$admin->updated_at}}</td>
                        
                        <td>
                        <button type="button" class="btn btn-info btn-lg" id="showEditModal" data-id="{{$admin->id}}">Edit</button>
                        <button type="button" class="btn btn-info btn-lg" id="showDeleteModal" data-id="{{$admin->id}}">Delete</button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
            <div class="clearfix">
                <div class="hint-text">Showing <b>{{$admins->count()}}</b> out of <b>{{$admins->total()}}</b> entries</div>
                {{ $admins->links() }}
            </div>
            </div>
@include('admin.admins.create')
@include('admin.admins.delete')
@include('admin.admins.edit')
</div>




@endsection

@section('css')
<style>
.divider-text {
    position: relative;
    text-align: center;
    margin-top: 15px;
    margin-bottom: 15px;
}
.divider-text span {
    padding: 7px;
    font-size: 12px;
    position: relative;   
    z-index: 2;
}
.divider-text:after {
    content: "";
    position: absolute;
    width: 100%;
    border-bottom: 1px solid #ddd;
    top: 55%;
    left: 0;
    z-index: 1;
}
</style>
@endsection

@section('js')
<script>
$(document).ready(function(){
  $("#showAddModal").click(function(){
    $("#addFormModal").modal("show");
  });
  

  $('#insert').on('submit',function (event) {
    event.preventDefault();
    var data=$(this).serialize();
    var url=$(this).attr('action');
    var method=$(this).attr('method');
   $.ajax({
     url:url,
     method:method,
     dataTy: 'json',
     data:data,     
     success:function(data){
       var tr=$('<tr/>');
       tr.prepend($("<td/>",{html :'<button type="button" class="btn btn-info btn-lg" id="showEditModal" data-id="'+data.id+'">Edit</button>'+
       ' <button type="button" class="btn btn-info btn-lg" id="showDeleteModal" data-id="'+data.id+'">Delete</button>'}))
      .prepend($("<td/>",{text:data.updated_at})).prepend($("<td/>",{text:data.email}))
       .prepend($("<td/>",{text:data.type}))
       .prepend($("<td/>",{text:data.name}))
       .prepend($("<td/>",{text:data.id}))
      $("#ajaxTable").prepend(tr);
       $('#addFormModal').modal('hide');
     },
     error: function (data) {
   $('#showErrorModal').html('');
   $.each(data.responseJSON.errors, function(key,error) {
     $('#showErrorModal').append('<div class="alert alert-danger">'+error+'</div');
 }); 
},

   });      
  });
  $(document).on('click','#showDeleteModal',function(){
    $('#deleteFormModal').modal('show');
   
    var id = $(this).data("id");
    var token = $("meta[name='csrf-token']").attr("content");
    $('#delete').click(function(){
    $.ajax(
    {
        url: "admins/"+id,
        type: 'DELETE',
        data: {
            "id": id,
            "_token": token,
        },
        success: function (){
         $('#tr'+id).remove();     
        }
    });
    $('#deleteFormModal').modal('hide');   
   
});
});
///////////////edit////////////////
$('body').delegate('#showEditModal', 'click', function(){

    var id = $(this).data("id");
    var token = $("meta[name='csrf-token']").attr("content");
  
        alert(id)
      
     
        $.ajax({
            url:"admins/"+id+"/edit",
            method:'get',
            data:{id:id,
                "_token": token
                },
            dataType:'json',
            success:function(data)
            { $('#edit').find('#name').val(data.name);
            $('#edit').find('#email').val(data.email);
            $('#edit').find('#type').val(data.type);
                $('#editFormModal').modal('show');
            }
        })
    });
    ////////////////////update//////////////////


    

    $('#edit').on('submit',function (e) {
    e.preventDefault();
    var data=$(this).serialize();
    var url=$(this).attr('action');
    var method=$(this).attr('method');
    alert(data)
console.log(data)
   $.ajax({
     url:url,
     method:method,
     dataTy: 'json',
     data:data,     
     success:function(data){
         
       var tr=$('<tr/>');
       tr.prepend($("<td/>",{html :'<button type="button" class="btn btn-info btn-lg" id="showEditModal" data-id="'+data.id+'">Edit</button>'+
       ' <button type="button" class="btn btn-info btn-lg" id="showDeleteModal" data-id="'+data.id+'">Delete</button>'}))
      .prepend($("<td/>",{text:data.updated_at})).prepend($("<td/>",{text:data.email}))
       .prepend($("<td/>",{text:data.type}))
       .prepend($("<td/>",{text:data.name}))
       .prepend($("<td/>",{text:data.id}))
      $("#ajaxTable"+data.id).replaceWith(tr);
       $('#editFormModal').modal('hide');
     },
     error: function (data) {
   $('#showErrorModal').html('');
   $.each(data.responseJSON.errors, function(key,error) {
     $('#showErrorModal').append('<div class="alert alert-danger">'+error+'</div');
 }); 
},

   });      
  });
});
</script>
@endsection