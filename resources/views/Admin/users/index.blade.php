@extends('dashboard.app')
@section('page-title')
Current Userss
@endsection

@section('content')
<div class="container">
<h2>user Dashboard</h2>
  <!-- Trigger the modal with a button -->
  <div>
  <button type="button" class="btn btn-info btn-lg" id="showAddModal">Add New User</button>
  </div>
  <div >
  <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>NAME</th>
                        <th>type </th>
                        <th>EMAIL </th>
                        <th>mobile </th>
                        <th>telephone </th>
                        <th>address </th>
                        <th>UPDATED AT</th>
                        <th>ACTIONS</th>
                    </tr>
                </thead>
                <tbody id="ajaxTable" >
                @php $i = 0 @endphp
                    @foreach($users as $user)
                    @php $i++ @endphp
                    <tr id="tr{{$user->id}}" >
                   
                        <td> {{$user->id}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->type}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->mobile}}</td>
                        <td>{{$user->telephone}}</td>
                        <td>{{$user->address}}</td>
                        <td>{{$user->updated_at}}</td>
                        
                        <td>
                        <button type="button" class="btn btn-info btn-lg" id="showEditModal" data-id="{{$user->id}}">Edit</button>
                        <button type="button" class="btn btn-info btn-lg" id="showDeleteModal" data-id="{{$user->id}}">Delete</button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
            <div class="clearfix">
                <div class="hint-text">Showing <b>{{$users->count()}}</b> out of <b>{{$users->total()}}</b> entries</div>
                {{ $users->links() }}
            </div>
            </div>
@include('admin.users.create')
@include('admin.users.delete')
@include('admin.users.edit')
</div>




@endsection

@section('css')
<style>
.divider-text {
    position: relative;
    text-align: center;
    margin-top: 15px;
    margin-bottom: 15px;
}
.divider-text span {
    padding: 7px;
    font-size: 12px;
    position: relative;   
    z-index: 2;
}
.divider-text:after {
    content: "";
    position: absolute;
    width: 100%;
    border-bottom: 1px solid #ddd;
    top: 55%;
    left: 0;
    z-index: 1;
}
</style>
@endsection

@section('js')
<script>
$(document).ready(function(){
  $("#showAddModal").click(function(){
    $("#addFormModal").modal("show");
  });
  

  $('#insert').on('submit',function (event) {
    event.preventDefault();
    var data=$(this).serialize();
    var url=$(this).attr('action');
    var method=$(this).attr('method');
   $.ajax({
     url:url,
     method:method,
     dataTy: 'json',
     data:data,     
     success:function(data){
       var tr=$('<tr/>');
       tr.prepend($("<td/>",{html :'<button type="button" class="btn btn-info btn-lg" id="showEditModal" data-id="'+data.id+'">Edit</button>'+
       ' <button type="button" class="btn btn-info btn-lg" id="showDeleteModal" data-id="'+data.id+'">Delete</button>'}))
      .prepend($("<td/>",{text:data.updated_at}))
      .prepend($("<td/>",{text:data.address}))
      .prepend($("<td/>",{text:data.telephone}))
      .prepend($("<td/>",{text:data.mobile}))
      .prepend($("<td/>",{text:data.email}))
       .prepend($("<td/>",{text:data.type}))
       .prepend($("<td/>",{text:data.name}))
       .prepend($("<td/>",{text:data.id}))
      $("#ajaxTable").prepend(tr);
       $('#addFormModal').modal('hide');
     },
     error: function (data) {
   $('#showErrorModal').html('');
   $.each(data.responseJSON.errors, function(key,error) {
     $('#showErrorModal').append('<div class="alert alert-danger">'+error+'</div');
 }); 
},

   });      
  });
  $(document).on('click','#showDeleteModal',function(){
    $('#deleteFormModal').modal('show');
   
    var id = $(this).data("id");
    var token = $("meta[name='csrf-token']").attr("content");
    $('#delete').click(function(){
    $.ajax(
    {
        url: "users/"+id,
        type: 'DELETE',
        data: {
            "id": id,
            "_token": token,
        },
        success: function (){
         $('#tr'+id).remove();     
        }
    });
    $('#deleteFormModal').modal('hide');   
   
});
});
///////////////edit////////////////
$('body').delegate('#showEditModal', 'click', function(){

    var id = $(this).data("id");
    var token = $("meta[name='csrf-token']").attr("content");
  
        alert(id)
      
     
        $.ajax({
            url:"users/"+id+"/edit",
            method:'get',
            data:{id:id,
                "_token": token
                },
            dataType:'json',
            success:function(data)
            { $('#edit').find('#name').val(data.name);
            $('#edit').find('#email').val(data.email);
            $('#edit').find('#type').val(data.type);
            $('#edit').find('#mobile').val(data.mobile);
            $('#edit').find('#telephone').val(data.telephone);
            $('#edit').find('#address').val(data.address);
  
                $('#editFormModal').modal('show');
                $('#edit').on('submit',function (e) {
    e.preventDefault();
    var data=$(this).serialize();
    var url=$(this).attr('action');
    var method=$(this).attr('method');
    
   $.ajax({
     url:url,
     method:method,
     dataTy: 'json',
     data:data,     
     success:function(data){
         
       var tr=$('<tr/>');
       tr.prepend($("<td/>",{html :'<button type="button" class="btn btn-info btn-lg" id="showEditModal" data-id="'+data.id+'">Edit</button>'+
       ' <button type="button" class="btn btn-info btn-lg" id="showDeleteModal" data-id="'+data.id+'">Delete</button>'}))
      .prepend($("<td/>",{text:data.updated_at})).prepend($("<td/>",{text:data.email}))
       .prepend($("<td/>",{text:data.type}))
       .prepend($("<td/>",{text:data.name}))
      $("#tr"+data.id).replaceWith(tr);
       $('#editFormModal').modal('hide');
     },
     error: function (data) {
   $('#showErrorModal').html('');
   $.each(data.responseJSON.errors, function(key,error) {
     $('#showErrorModal').append('<div class="alert alert-danger">'+error+'</div');
 }); 
},

   });      
  });
            }
        })
    });
    ////////////////////update//////////////////

   
    
});
</script>
@endsection