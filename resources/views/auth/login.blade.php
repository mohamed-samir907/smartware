<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="The Description for the Website">

        <!-- Theme CSS -->
        <link rel="stylesheet" href="{{ url('css/theme.min.css') }}">

        <title>{{ Config::get('APP_NAME', 'Nice Dental Care') }}</title>
    </head>
    <body class="d-flex align-items-center bg-auth border-top border-top-2 border-primary">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-md-6 offset-xl-2 offset-md-1 order-md-2 mb-5 mb-md-0">

                    <!-- Image -->
                    <div class="text-center">
                        <img src="{{ url('img/illustrations/doctor_kw5l.svg') }}" alt="Nice Dental Care Background" class="img-fluid">
                    </div>

                </div>
                <div class="col-12 col-md-5 col-xl-4 order-md-1 my-5">
                    <!-- Heading -->
                    <h1 class="display-4 text-center mb-3">
                        Sign in | {{ isset($url) ? ucwords($url) : "User or Doctor"}}
                    </h1>
                    <!-- Subheading -->
                    <p class="text-muted text-center mb-5">
                        Welcome to Nice Dental Care.
                    </p>

                    <!-- Form -->
                    @isset($url)
                    <form method="POST" action='{{ url("login/$url") }}' aria-label="{{ __('Login') }}">
                    @else
                    <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                    @endisset
                        <!-- Email address -->
                        @csrf

                        <div class="form-group">
                            <!-- Label -->
                            <label>Email Address</label>
                            <!-- Input -->
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="name@address.com" name="email" value="{{ old('email') }}" required autofocus>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <!-- Password -->
                        <div class="form-group">
                            <div class="row">
                                <div class="col">
                                    <!-- Label -->
                                    <label>Password</label>
                                </div>
                            </div> <!-- / .row -->
                            <!-- Input group -->
                            <div class="input-group input-group-merge">
                                <!-- Input -->
                                <input id="password" type="password" class="form-control form-control-appended{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Enter your password" required>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                <!-- Icon -->
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="fe fe-eye"></i>
                                    </span>
                                </div>
                            </div>
                            <!-- Remeber Me -->
                            <div class="form-group mt-4">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="remember">{{ __('Remember Me') }}</label>
                                </div> 
                            </div>
                        </div>
                        <!-- Submit -->
                        <button class="btn btn-lg btn-block btn-primary mb-3">
                            Sign in
                        </button>
                    </form>
                </div>
            </div> <!-- / .row -->
        </div> <!-- / .container -->
    </body>
</html>