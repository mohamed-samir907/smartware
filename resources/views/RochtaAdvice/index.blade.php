@extends('layouts.app')
@section('page-title')
Current Advicesss
@endsection

@section('content')
<div class="container">
<h2>user Dashboard</h2>
  <!-- Trigger the modal with a button -->
  <div>
  <button type="button" class="btn btn-info btn-lg" id="showAddModal">Add New Illness</button>
  </div>
  <div >
  <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>ِِِِِِAdvice</th>
                        <th>ACTIONS</th>
                    </tr>
                </thead>
                <tbody id="ajaxTable" >
                @php $i = 0 @endphp
                    @foreach($advices as $advice)
                    @php $i++ @endphp
                    <tr id="tr{{$advice->id}}" >
                   
                        <td> {{$advice->advice}}</td>
                       
                        <td>
                        <button type="button" class="btn btn-info btn-lg" id="showEditModal" data-id="{{$advice->id}}">Edit</button>
                        <button type="button" class="btn btn-info btn-lg" id="showDeleteModal" data-id="{{$advice->id}}">Delete</button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
            <div class="clearfix">
                <div class="hint-text">Showing <b>{{$advices->count()}}</b> out of <b>{{$advices->total()}}</b> entries</div>
                {{ $advices->links() }}
            </div>
            </div>
@include('advices.create')
@include('advices.delete')
@include('advices.edit')
</div>




@endsection

@section('css')
<style>
.divider-text {
    position: relative;
    text-align: center;
    margin-top: 15px;
    margin-bottom: 15px;
}
.divider-text span {
    padding: 7px;
    font-size: 12px;
    position: relative;   
    z-index: 2;
}
.divider-text:after {
    content: "";
    position: absolute;
    width: 100%;
    border-bottom: 1px solid #ddd;
    top: 55%;
    left: 0;
    z-index: 1;
}
</style>
@endsection

@section('js')
<script>
$(document).ready(function(){
  $("#showAddModal").click(function(){
    $("#addFormModal").modal("show");
  });
  

  $('#insert').on('submit',function (event) {
    event.preventDefault();
    var data=$(this).serialize();
    var url=$(this).attr('action');
    var method=$(this).attr('method');
   $.ajax({
     url:url,
     method:method,
     dataTy: 'json',
     data:data,     
     success:function(data){
       var tr=$('<tr/>');
       tr.prepend($("<td/>",{html :'<button type="button" class="btn btn-info btn-lg" id="showEditModal" data-id="'+data.id+'">Edit</button>'+
       ' <button type="button" class="btn btn-info btn-lg" id="showDeleteModal" data-id="'+data.id+'">Delete</button>'}))
      .prepend($("<td/>",{text:data.advice}))
      $("#ajaxTable").prepend(tr);
       $('#addFormModal').modal('hide');
     },
     error: function (data) {
   $('#showErrorModal').html('');
   $.each(data.responseJSON.errors, function(key,error) {
     $('#showErrorModal').append('<div class="alert alert-danger">'+error+'</div');
 }); 
},

   });      
  });
  $(document).on('click','#showDeleteModal',function(){
    $('#deleteFormModal').modal('show');
   
    var id = $(this).data("id");
    var token = $("meta[name='csrf-token']").attr("content");
    $('#delete').click(function(){
    $.ajax(
    {
        url: "advices/"+id,
        type: 'DELETE',
        data: {
            "id": id,
            "_token": token,
        },
        success: function (){
         $('#tr'+id).remove();     
        }
    });
    $('#deleteFormModal').modal('hide');   
   
});
});
///////////////edit////////////////
$('body').delegate('#showEditModal', 'click', function(){

    var id = $(this).data("id");
    var token = $("meta[name='csrf-token']").attr("content");
  
        alert(id)
      
     
        $.ajax({
            url:"/advices/"+id+"/edit",
            method:'get',
            data:{id:id,
                "_token": token
                },
            dataType:'json',
            success:function(data)
            { $('#edit').find('#name').val(data.advice);
         
  
                $('#editFormModal').modal('show');
            }
        })
    });
    ////////////////////update//////////////////

    $('#edit').on('submit',function (e) {
    e.preventDefault();
    var data=$(this).serialize();
    var url=$(this).attr('action');
    var method=$(this).attr('method');
   $.ajax({
     url:url,
     method:method,
     dataTy: 'json',
     data:data,     
     success:function(data){
         
       var tr=$('<tr/>');
       tr.prepend($("<td/>",{html :'<button type="button" class="btn btn-info btn-lg" id="showEditModal" data-id="'+data.id+'">Edit</button>'+
       ' <button type="button" class="btn btn-info btn-lg" id="showDeleteModal" data-id="'+data.id+'">Delete</button>'}))
       .prepend($("<td/>",{text:data.advice}))
      $("#ajaxTable tr#"+data.id).replaceWith(tr);
       $('#editFormModal').modal('hide');
     },
     error: function (data) {
   $('#showErrorModal').html('');
   $.each(data.responseJSON.errors, function(key,error) {
     $('#showErrorModal').append('<div class="alert alert-danger">'+error+'</div');
 }); 
}

   });      
  });
    
});
</script>
@endsection