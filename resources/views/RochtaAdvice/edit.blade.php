<div class="modal fade" id="editFormModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">advicer</h4>
        </div>
       
       
        <form id="edit" method="post" action="advices/{{$advice->id}}">
        @csrf
   @method('PATCH')
                        <div class="modal-body">
                        <div class="form-group row">
                            <label for="advice" class="col-md-4 col-form-label text-md-right">Advice</label>

                            <div class="col-md-6">
                                <input id="advice" type="text" class="form-control" name="advice" " required autofocus>

                                @if ($errors->has('advice'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('advice') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                      
                        
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button  type="submit" class="btn btn-default" >Update</button>
        </div>
        </form>
      </div>
      
    </div>
  </div>
  