<div class="modal fade" id="addFormModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Register New advice</h4>
        </div>
       
       
        <form id="insert" method="POST" action="{{ route('advices.store') }}" >
        
                        @csrf
                        @include('errors')
                        <div class="modal-body">
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Advice</label>

                            <div class="col-md-6">
                                <input id="advice" type="text" class="form-control" name="advice" " required autofocus>

                                @if ($errors->has('advice'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('advice') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button  id="Add" type="submit" class="btn btn-default" >Add</button>
        </div>
        </form>
      </div>
      
    </div>
  </div>
  