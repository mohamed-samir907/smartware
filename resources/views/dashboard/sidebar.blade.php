<div class="be-left-sidebar">
    <div class="left-sidebar-wrapper">
        <a class="left-sidebar-toggle" href="#">
            @yield('page-title')
        </a>
        <div class="left-sidebar-spacer">
            <div class="left-sidebar-scroll">
                <div class="left-sidebar-content">
                    <ul class="sidebar-elements">
                        <li class="divider">Menu</li>
                        <li>
                            <a href="index-2.html">
                                <i class="icon mdi mdi-home"></i>
                                <span>Dashboard</span>
                            </a>
                        </li>
                        <li class="parent">
                            <a href="#">
                                <i class="icon mdi mdi-layers"></i>
                                <span>Pages</span>
                            </a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="{{url('adminRegister')}}">Admin Page</a>
                                </li>
                                <li>
                                    <a href="{{url('userRegister')}}">Users Page</a>
                                </li>
                            </ul>
                        </li>
                        
                        <li class="divider">Settings</li>
                        <li class="parent">
                            <a href="#">
                                <i class="icon mdi mdi-inbox"></i>
                                <span>Email</span>
                            </a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="pages-blank.html">Blank Page</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>