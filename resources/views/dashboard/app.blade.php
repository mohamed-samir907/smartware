<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="img/logo-fav.png">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="stylesheet" href="{{ url('lib/perfect-scrollbar/css/perfect-scrollbar.css') }}"/>
    <link rel="stylesheet" href="{{ url('lib/material-design-icons/css/material-design-iconic-font.min.css') }}"/>
    <link rel="stylesheet" href="{{ url('lib/mprogress/css/mprogress.min.css') }}"/>
    <link rel="stylesheet" href="{{ url('css/app.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    
    @yield('css')
</head>
<body>
    <div class="be-wrapper">
        {{-- Navbar --}}
        @include('dashboard.navbar')
        {{-- Sidebar --}}
        @include('dashboard.Sidebar')
        <div class="be-content">
            @yield('content')
        </div>
    </div>
    <script src="{{ url('lib/jquery/jquery.min.js') }}"></script>
    <script src="{{ url('lib/perfect-scrollbar/js/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ url('lib/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ url('js/app.js') }}"></script>
    <script src="{{ url('lib/mprogress/js/mprogress.min.js') }}"></script>
    @yield('js')
    <script>
        $(document).ready(function(){
            //-initialize the javascript
            App.init();
            // App.ajaxLoader();
            var e = new Mprogress;
            e.start();
            setTimeout(function(){
                e.end()
            },2);
        });
    </script>
</body>
</html>
