<?php

/**
 * Landing Page Routes
 */
Route::get('/', function () {
    return view('welcome');
});
/**
 * Authentication Routes
 */
Auth::routes();
Route::get('/login/admin', 'Auth\LoginController@showAdminLoginForm')->name('loginAdmin');
Route::post('/login/admin', 'Auth\LoginController@adminLogin');

/**
 * User and Doctor Routes with Auth Middleware (Require Authentication)
 */
Route::group(['middleware' => 'auth'], function() {

	/**
	 * Home Routes
	 */
	// Route::view('/home', 'home');
	Route::view('/doctor', 'Doctor.index');
	Route::view('/user', 'Doctor.index');
	
});

/**
 * Admin Routes with Auth Middleware (Require Authentication)
 */
Route::group(['middleware' => 'auth:admin'], function() {
	Route::view('/admin', 'admin.index');
	Route::resource('admin/admins','Admin\Admins\AdminController' );
Route::resource('admin/users','Admin\Users\UserController');
});
// Route::resource('userRegister','user\UserRegisterController');

Route::resource('advices','Advices\AdvicesController');
Route::resource('illness','Illness\IllnessController');